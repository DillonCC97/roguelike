/*
  Rogue-like prototype
  Compile with:
  g++ -o prototype prototype.cpp -lncurses
*/

#include <iostream>
#include <cstdlib>
#include <string>
#include <curses.h>
#define MAPSIZEY 20
#define MAPSIZEX 30

using namespace std;

// TODO: properly document
// TODO: make item and entity references modular
// TODO: clean up unused functions
// TODO: clean up public reference chains

class Entity {
private:
  char avatar;
  int health;
  int posX;
  int posY;
  int str;
  int def;
  int dex;
  int itl;
  int killcount;
  
public:
  int deltay = 0, deltax = 0;
  void init(int newy, int newx, int newhp, char newavatar) {
    setpos(newx, newy);
    sethealth(newhp);
    setavatar(newavatar);
    killcount = 0;
  }

  void setpos(int x, int y){
    posX = x;
    posY = y;
  }

  int getposX(){
    return posX;
  }

  int getposY(){
    return posY;
  }

  void sethealth(int x){
    health = x;
  }

  int gethealth(){
    return health;
  }

  void addkill(){
    killcount += 1;
  }

  void setavatar(char x){
    avatar = x;
  }

  char getavatar(){
    return avatar;
  }

  void drawplayer(char (&map)[MAPSIZEY][MAPSIZEX]) {
    map[getposY()][getposX()] = getavatar();
  }

  void move(int deltay, int deltax) {
    if(mvinch(getposY()+deltay,getposX()+deltax) =='.' || mvinch(getposY() + deltay, getposX() + deltax) == '#' || islower(mvinch(getposY() + deltay, getposX() + deltax))){
      setpos(getposX() + deltax, getposY() + deltay);
    }
  }

  void move(int deltay, int deltax, Entity &enemy) {
    if(mvinch(getposY()+deltay,getposX()+deltax)=='.' || mvinch(getposY() + deltay, getposX() + deltax) == '#' || islower(mvinch(getposY() + deltay, getposX() + deltax))) {
      setpos(getposX() + deltax, getposY() + deltay);
    }
    if(mvinch(getposY() +deltay, getposX() +deltax)=='M') {
      hit(enemy);
    }
  }
  void hit(Entity &referentity) {
    referentity.sethealth(referentity.gethealth() - 1);
  }

  void runAI(char (&map)[MAPSIZEY][MAPSIZEX],Entity &refplayer) {
    if(refplayer.getposX() == posX) {
      deltax=0;
    } if(refplayer.getposY() == posY) {
      deltay=0;
    }
    if(refplayer.getposX() < posX) {
      deltax=-1;
    } else if(refplayer.getposX() > posX) {
      deltax=1;
    } if(refplayer.getposY() < posY) {
      deltay=-1;
    } else if(refplayer.getposY() > posY) {
      deltay=+1;
    }
    if (map[posY + deltay][posX + deltax]=='@') {
      hit(refplayer);
    } else {
      move(deltay,deltax);
    }
  }

  void printstats(int row) {
    mvprintw(row, 0, "HP: %3d", health);
    if(avatar == '@'){
      mvprintw(row+1, 0, "KILLS: %3d", killcount);
      mvprintw(row+2, 0, "x: %3d", getposX());
      mvprintw(row+3, 0, "y: %3d", getposY());
    } else {
      mvprintw(row+1, 0, "x: %3d", getposX());
      mvprintw(row+2, 0, "y: %3d", getposY());
    }
  }
};

class Screen {
private:
public:
  char map[MAPSIZEY][MAPSIZEX];
  void initmap() {
    for(int mapy = 0; mapy <= MAPSIZEY; mapy++) {
      for( int mapx = 0; mapx <=MAPSIZEX; mapx++) {
        if(mapy == 0 || mapx == 0 || mapx == MAPSIZEX || mapy == MAPSIZEY) {
          map[mapy][mapx] = '+';
        } else {
          map[mapy][mapx] = '.';
        }
      }
    }
  }
  void drawmap() {
    for(int mapy = 0; mapy <= MAPSIZEY; mapy++) {
      for( int mapx = 0; mapx <=MAPSIZEX; mapx++) {
        mvaddch(mapy, mapx, map[mapy][mapx]);
      }
    }
  }
};

class Item {
private:
  int posX;
  int posY;
  int deltahealth;
  int deltastr;
  int deltadef;
  int deltadex;
  int deltaitl;
  char avatar;
public:
  void init(int x, int y, int health, int str, int def, int dex, int itl, char ava){
    deltahealth = health;
    deltastr = str;
    deltadef = def;
    deltadex = dex;
    deltaitl = itl;
    avatar = ava;
    posX = x;
    posY = y;
  }

  int getposX(){
    return posX;
  }

  int getposY(){
    return posY;
  }

  void setpos(){
    posY = rand() % 18 + 1;
    posX = rand() % 28 + 1;
  }

  void consume(Entity &referentity){
    referentity.sethealth(referentity.gethealth() + deltahealth); // add additional stat augments
  }

  void drawitem(char (&map)[MAPSIZEY][MAPSIZEX]){
    map[posY][posX] = avatar;
  }
};

int main() {
  initscr();
  raw();
  curs_set(0);
  noecho();
  Screen screen;
  screen.initmap();
  Entity player;
  player.init(1,1,10, '@');
  Entity enemy;
  enemy.init(15,15,3,'M');
  Item potion;
  potion.init(rand() % 18 + 1, rand() % 28 + 1, 2, 1, 1, 1, 1, 'p'); // replace with random location
  char key;
  int deltax = 0, deltay = 0;
  do {
    switch (key) {
    case 'q': {
      deltay=-1; deltax=-1;
      break;
    }
    case 'w': {
      deltay=-1; deltax=0;
      break;
    }
    case 'a': {
      deltay=0; deltax=-1;
      break;
    }
    case 's': {
      deltay=1; deltax=0;
      break;
    }
    case 'd': {
      deltay=0; deltax=1;
      break;
    }
    }
    player.move(deltay,deltax,enemy);
    screen.initmap();
    potion.drawitem(screen.map);
    if (potion.getposX() == player.getposX() && potion.getposY() == player.getposY()) {
      potion.consume(player);
      potion.setpos();
    }
    player.drawplayer(screen.map);
    //screen.drawmap();
    enemy.runAI(screen.map, player);
    enemy.drawplayer(screen.map);
    screen.drawmap();
    //screen.drawstats();
    player.printstats(24);
    enemy.printstats(28);
    if (enemy.gethealth()<1) {
      player.addkill();
      if(player.getposY() < 10) {
        enemy.init(15,15, 4,'M');
      } else {
        enemy.init(5,5,4,'M');
      }
    }
    if (player.gethealth()<1) {
      mvprintw(21,0,"YOU LOST THE GAME! ESC TO QUIT, PRESS ANY KEY TO RESTART");
      getch();
      player.init(1,1,10,'@');
      enemy.init(15,15,3,'M');
      clear();
    }
  } while ((27!=(key=getch())));
  refresh();
  endwin();
  return 0;
}

