#include <curses.h>
#include <ctime>
#include <cmath>
#define MAPSIZEY 20
#define MAPSIZEX 30
class Entity {
private:
public:
  int x=1, y=1, deltay=0, deltax=0, hp=10;
  char looks='@';
  void init(int newy, int newx, int newhp, char newlooks) {
    y=newy;
    x=newx;
    hp=newhp;
    looks=newlooks;
  }
  void drawme(char (&map)[MAPSIZEY][MAPSIZEX]) {
    map[y][x]=looks;
  }
  void move(int deltay, int deltax) {
    if(mvinch(y+deltay,x+deltax)=='.') {
      y=y+deltay;
      x=x+deltax;
    }
  }
  void move(int deltay, int deltax, Entity &enemy) {
    if(mvinch(y+deltay,x+deltax)=='.') {
      y=y+deltay;
      x=x+deltax;
    }
    if(mvinch(y+deltay,x+deltax)=='M') {
      hit(enemy);
    }
  }
  void printstats(int row) {
    mvprintw(row,0, "hp: %2d", hp);
    mvprintw(row+1,0, "x: %3d", x);
    mvprintw(row+2,0, "y: %3d", y);
  }
  void runAI(char (&map)[MAPSIZEY][MAPSIZEX],Entity &refplayer) {
    if(refplayer.x==x) {
      deltax=0;
    } if(refplayer.y==y) {
      deltay=0;
    }
    if(refplayer.x<x) {
      deltax=-1;
    } else if(refplayer.x>x) {
      deltax=1;
    } if(refplayer.y<y) {
      deltay=-1;
    } else if(refplayer.y>y) {
      deltay=+1;
    } 
    if (map[y+deltay][x+deltax]=='@') {
      hit(refplayer);
    } else {
      move(deltay,deltax);
    }
  }
  void hit(Entity &refentity) {
    refentity.hp-=1;
  }
};
class Screen {
private:
public:
  char map[MAPSIZEY][MAPSIZEX];
  void initmap() {
    for (int mapy=0;mapy<=MAPSIZEY;mapy++) {
      for (int mapx=0;mapx<=MAPSIZEX;mapx++) {
        if (mapy==0 || mapx==0 || mapy==MAPSIZEY || mapx==MAPSIZEX)
          map[mapy][mapx]='#';
        else
          map[mapy][mapx]='.';
      }
    }
  }
  void drawmap() {
    for (int mapy=0;mapy<=MAPSIZEY;mapy++) {
      for (int mapx=0;mapx<=MAPSIZEX;mapx++) {
        mvaddch(mapy, mapx, map[mapy][mapx]);
      }
    }
  }
    
  void drawstats() {
    mvprintw(22,0, "MAPSIZEY: %d", MAPSIZEY);
    mvprintw(23,0, "MAPSIZEX: %d", MAPSIZEX);
  }
};
int main() {
  initscr();
  raw();
  curs_set(0);
  noecho();
  Screen screen;
  screen.initmap();
  Entity player;
  Entity enemy;
    
  enemy.init(15,15, 1,'M');
  char key;
  int deltay=0,deltax=0;
  do {
    switch (key) {
    case 'q': {
      deltay=-1; deltax=-1;
      break;
    } case 'w': {
        deltay=-1; deltax=0;
        break;
      } case 'e': {
          deltay=-1; deltax=1;
          break;
        } case 'a': {
            deltay=0; deltax=-1;
            break;
          } case 's': {
              deltay=0; deltax=0;
              break;
            } case 'd': {
                deltay=0; deltax=1;
                break;
              } case 'z': {
                  deltay=1; deltax=-1;
                  break;
                } case 'x': {
                    deltay=1; deltax=0;
                    break;
                  } case 'c': {
                      deltay=1; deltax=1;
                      break;
                    }
    }
    player.move(deltay,deltax,enemy);
    screen.initmap();
    player.drawme(screen.map);
    screen.drawmap();
    enemy.runAI(screen.map, player);
    enemy.drawme(screen.map);
    screen.drawmap();
    screen.drawstats();
    player.printstats(24);
    enemy.printstats(28);
    if (enemy.hp<1) {
      if(player.y<10) {
        enemy.init(15,15, 3,'M');
      } else {
        enemy.init(5,5,3,'M');
      }
    }
    if (player.hp<1) {
      mvprintw(21,0,"YOU LOST THE GAME! ESC TO QUIT, A COUPLE OF ANY OTHER KEY TO RESTART");
      getch();
      getch();
      getch();
      player.init(1,1,10,'@');
      enemy.init(15,15,3,'M');
      clear();
    }
  } while ((27!=(key=getch())));
  refresh();
  endwin();
  return 0;
}
